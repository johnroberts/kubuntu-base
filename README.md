# Summary
Provision and manage a development or general-purpose computing environment based on Linux Mint (currently 21.2 Cinnamon edition). This can be a Vagrant/Virtualbox VM, or any other compatible host such as a non-Vagrant VM or bare metal.

# Usage
## Installing Project Dependencies
This project uses external Ansible roles and collections managed via `ansible-galaxy`. These need to be installed to the project directory after cloning. To do this make sure `git` and `ansible-galaxy` are available, then:
```sh
cd this-repo
ansible-galaxy role install -r roles/requirements.yml
ansible-galaxy collection install -r ansible_collections/requirements.yml
```

These commands use [role](roles/requirements.yml) and [collection](ansible_collections/requirements.yml) requirement files (plus [ansible.cfg](ansible.cfg)). For more info see [StackOverflow](https://stackoverflow.com/a/55774721) and [Jeff Geerling's post](https://www.jeffgeerling.com/blog/2020/ansible-best-practices-using-project-local-collections-and-roles).

## Software Selection
This project installs an opinionated set of default software. View and adjust this with variables named as `install_softwarename` in `vars/*.yml`, or by editing [playbook-vagrant.yml](playbook-vagrant.yml)/[playbook-localhost.yml](playbook-localhost.yml).

Most software and settings are shared between Vagrant and non-Vagrant usage. There are a few exceptions, such as not installing Virtualbox into a VM and different default usernames:
- [Main/shared variables](vars/setup-vars-shared.yml) (used by both playbooks)
- [Vagrant-specific variables](vars/setup-vars-vm-specific.yml) (used by [playbook-vagrant.yml](playbook-vagrant.yml))
- [Non-Vagrant-specific variables](vars/setup-vars-localhost-specific.yml) (used by [playbook-localhost.yml](playbook-localhost.yml))

## Vagrant/Virtualbox VM
Clone this repo, install the project's dependencies, install Vagrant and Virtualbox if you don't have them, then:
```sh
cd this-repo
vagrant up
```

[playbook-vagrant.yml](playbook-vagrant.yml) provisions the Vagrant VM.

## Non-Vagrant Host
Update `docker_users: ["username"]` (in [vars/setup-vars-localhost-specific.yml](vars/setup-vars-localhost-specific.yml)) to the trusted user you'll be using Docker with. This user will be added to the `docker` group, [effectively granting this user root privileges](https://docs.docker.com/engine/install/linux-postinstall/). This configuration ensures you don't need to prepend each `docker` command with `sudo`.

Then follow [these steps](docs/localhost-setup-without-vagrant.md) to set up a non-Vagrant host (bare metal, non-Vagrant VM, ...) using the [localhost playbook](playbook-localhost.yml).

# Creating Vagrant Custom Base Box
Follow [these steps](docs/mint-basebox-build-notes.md) to create your own Vagrant base box using Virtualbox. That's how [this project's base box](https://app.vagrantup.com/CJJR/boxes/LinuxMint21) was created.