# Manual tweaks after using vm-base playbook on bare metal
[ ] Update Firefox profile: add Bitwarden + I still don't care about cookies + add sidebery, customize new-tab page
[ ] Create ansiblerole-cinnamon-opinionated
    [ ] create and named workspaces, like this screenshot:
![Image stored in img subdirectory](img/cinnamon-workspaces.png)
    [ ] wallpaper
    [ ] flameshot autostart
    [ ] save session on logout
    [ ] qredshift applet config
    [ ] set system default monospace font to size 20
    [ ] gnome-terminal: prefs -> global -> disable show menubar by default in new terminals

# Completed
[x] Add ctop to command-line tools
[x] Select + add SQLite DB GUI tool
[x] Add Virtualbox
[x] Add Mullvad
[x] Add Zoom
[x] Add Veracrypt
[x] Add ungoogled chromium
[x] Go through Linux Mint install and add stuff that's not already in the VM
[x] Change base OS to Linux Mint 21.1
[x] Create personal Linux Mint 21.1 base box
[x] Test Firefox role
[x] Add Firefox profile w/ extensions preinstalled
[x] Add fancy CLI
[x] Add OnionShare
[x] Add Packer
[x] Add Terraform
[x] Add toggleable apt upgrade
[x] Add tor browser
[x] Add Signal
[x] Add Vagrant
[x] Add Ansible
[x] Add Discord
[x] Add Burp Suite
[x] Add Anki
[x] Add Keybase
[x] Add remote desktop software (Anydesk, Teamviewer)
[x] Fix Burp Suite so it doesn't download script each time
[x] Split Firefox into its own role

# Discarded
[ ] Add markdown editor (Obsidian) -- discarded in favor of VSCode
[ ] Add KDE customizations (wallpaper, theme, launchers, menu, window decos, etc) -- discarded in favor of Cinnamon