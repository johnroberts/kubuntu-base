# Outline
- Download ISO
- Create VM in Virtualbox + install 
- Reboot
- Install Virtualbox guest additions, reboot
- Install updates
- Install OpenSSH server
- Set up `vagrant/vagrant` user: default password, passwordless sudo, default Vagrant public key
- Install Ansible and dependencies (so `ansible_local` provisioner can work later from `Vagrantfile`)
- Shut down VM
- Package with `vagrant box`

# Detailed Steps
## Linux Mint manual install notes
- Download official Linux Mint 21.1 Cinnamon 64-bit desktop ISO, use this is base
- Name VM as `Base-LinuxMint21`
- Boot order hard drive first
- Manually add live ISO as IDE device
- 150GB VMDK dynamically expanding virtual drive
- English/US keyboard layout
- Minimal installation
- Installed multimedia codecs
- Update packages during installation
- Guided use entire disk defaults
- Set timezone to UK time (for UTC)
- `vagrant/vagrant` user created, configured for auto GUI login
- Machine name/hostname: `mint-base`
- VM settings -> general -> Advanced -> Shared clipboard -> bidirectional
- VM settings -> general -> Advanced -> Drag n drop -> bidirectional

## Install Virtualbox Guest Additions notes
Do these after initial install + reboot.

- Need to install prereqs before guest additions (to build and load the virtualbox kernel modules): `sudo apt install build-essential dkms linux-headers-generic`
- Then insert guest additions CD, mount it, and install it by executing `VBoxLinuxAdditions.run` from the mounted guest additions ISO image as root

## Install updates
Apply all updates available through Update Manager (`mintupdate`)

## Vagrant prereqs notes
- `vagrant/vagrant` user exists (happens during manual install)
- `vagrant` user is capable of passwordless sudo
`sudo visudo`, then add and save this content:
```
# Allow vagrant user passwordless sudo
vagrant ALL=(ALL) NOPASSWD: ALL
```
- Install and start openssh server: `sudo apt install -y openssh-server`
- `vagrant` user trusts insecure Vagrant public key in `~/.ssh/authorized_keys`:
```bash
rm -Rf /home/vagrant/.ssh
mkdir /home/vagrant/.ssh
wget -O /home/vagrant/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub
chown -R vagrant:vagrant /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
```

# Clean the box
Execute (scripts/basebox-cleanup.sh)[scripts/basebox-cleanup.sh]

# Package the box
Once prereqs are done shut down the guest, then run this on the host with `vagrant` and `virtualbox`:
```bash
mkdir ~/vagrant-boxes
cd ~/vagrant-boxes
vagrant package --base Base-LinuxMint21 --output Base-LinuxMint21.box
vagrant box add Base-LinuxMint21.box --name personal-boxes/LinuxMint21
```

After adding the box to Vagrant, should be able to use it in a `Vagrantfile` and/or upload it to Vagrant Cloud.

# Resources
- Adapting this to Kubuntu: https://oracle-base.com/articles/vm/create-a-vagrant-base-box-virtualbox
- https://app.vagrantup.com/aaronvonawesome/boxes/linux-mint-21-cinnamon