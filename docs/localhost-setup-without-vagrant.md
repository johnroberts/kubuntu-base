# Summary
Bootstrap an environment without Vagrant-based provisioning (i.e. in a non-Vagrant VM or on bare metal).

# Bootstrapping Process
As a sudo-capable user:
```sh
# Install prereqs
sudo apt install -y ansible git

# Clone repo
mkdir -p git/VMs && cd git/VMs
git clone https://gitlab.com/johnroberts/vm-base.git
cd vm-base

# Install ansible roles + collections
ansible-galaxy install -r roles/requirements.yml
ansible-galaxy collection install -r ansible_collections/requirements.yml

# Run the localhost playbook to set up localhost
ansible-playbook -K --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 playbook-localhost.yml
```

# Updating
As a sudo-capable user:
```sh
cd ~/git/VMs/vm-base

# Ensure roles are on current versions
scripts/refresh-roles.sh

# Run localhost playbook
ansible-playbook -K --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 playbook-localhost.yml
```