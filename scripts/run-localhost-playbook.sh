#!/bin/bash

# Assumes project-root working directory
ansible-playbook -K --connection=local --inventory 127.0.0.1, --limit 127.0.0.1 playbook-localhost.yml