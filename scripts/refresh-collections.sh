#!/bin/bash

# Run this from project root to refresh collections stored in upstream repos

# Remove project-local collections
rm -rf ansible_collections/community*

# Install project collections
ansible-galaxy collection install -r ansible_collections/requirements.yml