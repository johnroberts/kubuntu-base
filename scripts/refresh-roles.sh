#!/bin/bash

# Run this from project root to refresh roles stored in upstream repos

# Remove roles installed from git repos
rm -rf roles/cli-tools \
        roles/fancy-cli \
        roles/firefox \
        roles/git \
        roles/gui-tools \
        roles/manage-apt-updates \
        roles/manage-software-distribution-frameworks

# Install project roles (refresh roles stored in upstream repos)
ansible-galaxy install -r roles/requirements.yml