#!/bin/bash

##### Summary #####
# Purpose: prepare a Linux machine for packaging as a base box
# Execute this just before packaging the box with vagrant
# Adapted from Vagrant Cloud box: aaronvonawesome/linux-mint-21-cinnamon

##### Apt #####
printf "STEP: Clean up APT\n"
sudo apt-get clean -y
sudo apt-get autoclean -y
sudo apt autoremove -y

##### Empty Space #####
printf "STEP: Zero out all the empty space\n"
sudo dd if=/dev/zero of=/tmp/EMPTY bs=1M
sudo rm -rf /tmp/*

##### Shell History #####
printf "STEP: Delete shell history\n"
unset HISTFILE
sudo rm -f /root/.bash_history
rm -f /home/vagrant/.bash_history